Partner nyilvántartó

szükséges modulok telepítése `npm i`

client mappában a Reactjs indítása: `npm start`

server nodejs backend `npm start`

Hibák: adatfrissítésnél a defaultValue az input mezőkben nem kerül be autómatikusan a változókba. Erre sajnos még nem sikerült megoldást találnom.
Az extra feladatok hiányoznak. Az excel modul nem akart működni. A keresés implementálására pedig már nem maradt idő.
Került be pár extra input ellenörzés (cégjegyzékszám, adószám,stb csak szám lehet).
Az sqlite hibaüzenetei sajnos csak a szerver-konzolon olvashatók, erre sem volt időm hogy implementáljam.
