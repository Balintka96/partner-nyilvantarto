import React, {useState, useEffect} from "react";
import Modal from 'react-bootstrap/Modal'
import './App.css';
import axios from 'axios';

function findWithAttr(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

function isInteger(value) {
  return /^\d+$/.test(value);
}



function MyVerticallyCenteredModal(props) {

  const [name, setName] = useState('');
  const [comType, setComType] = useState('');
  const [taxNum, setTaxNum] = useState('');
  const [comNum, setComNum] = useState('');
  const [city, setCity] = useState('');
  const [addrs, setAddrs] = useState('');
  const [telNum, setTelNum] = useState('');
  const [accNum, setAccNum] = useState('');
  const [info, setInfo] = useState('');

const newData = () => {
    if ((findWithAttr(props.citylist,"name",city) === -1) && (city !== ''))
    {
      axios.post("http://localhost:3001/api/new_city", {name: city});
    }
    if ((findWithAttr(props.comptypelist,"name",comType) === -1) && (city !== ''))
    {
      axios.post("http://localhost:3001/api/new_comtype", {name: comType});
    }
    if (name === '' || city === '') {
      alert("A Név és Település mezők kitöktése kötelező!");
      return false;
    }
    if (false===((isInteger(taxNum) || taxNum === "") && (isInteger(comNum) || comNum === "") && (isInteger(accNum) || accNum === "")))
    {
      alert("Helyes adatokat adjon meg!");
      return false;
    }
    axios.post("http://localhost:3001/api/new_dat", {
      name: name,
      comType: comType,
      taxNum: taxNum,
      comNum: comNum,
      city: city,
      addrs: addrs,
      telNum: telNum,
      accNum: accNum,
      info: info}).then(
        props.onHide(),
        window.setTimeout(function(){window.location.reload()}, 300));
    };

const updateData = () => {
      //console.log(props.citylist);
      if ((findWithAttr(props.citylist,"name",city) === -1) && (city !== ''))
      {
        axios.post("http://localhost:3001/api/new_city", {name: city});
      }
      if ((findWithAttr(props.comptypelist,"name",comType) === -1) && (city !== ''))
      {
        axios.post("http://localhost:3001/api/new_comtype", {name: comType});
      }
      if (name === '' || city === '') {
        alert("A Név és Település mezők kitöktése kötelező!");
        return false;
      }
      if (false===((isInteger(taxNum) || taxNum === "") && (isInteger(comNum) || comNum === "") && (isInteger(accNum) || accNum === "")))
      {
        alert("Helyes adatokat adjon meg!");
        return false;
      }
      axios.post("http://localhost:3001/api/edit", {
        name: name,
        comType: comType,
        taxNum: taxNum,
        comNum: comNum,
        city: city,
        addrs: addrs,
        telNum: telNum,
        accNum: accNum,
        info: info,
        id: props.id}).then(
          props.onHide(),
          window.setTimeout(function(){window.location.reload()}, 300));
        //console.log(props.id);
      };

  const index = findWithAttr(props.datbase,"id",props.id);
  //console.log(index);
  if (props.id == null){
    return (
      <Modal id="modal"
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Új adat felvétele
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
            <thead>
              <tr>
                <th>Név</th>
                <th>Cégforma</th>
                <th>Adószám</th>
                <th>Cégjegyzékszám</th>
                <th>Település</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td><input type="text" id="name" onChange={(e) => {setName(e.target.value)}}/></td>
                <td>
                    <input list="comp" onChange={(e) => {setComType(e.target.value)}}/>
                    <datalist id="comp">
                      <option value="" selected disabled hidden>-</option>
                      {props.comptypelist.map(({ name: value }) => <option value={value}>{value}</option>)}
                    </datalist>
                </td>
                <td><input type="text" id="tax" onChange={(e) => {setTaxNum(e.target.value)}}/></td>
                <td><input type="text" id="cpyn" onChange={(e) => {setComNum(e.target.value)}}/></td>
                <td>
                    <input list="city" onChange={(e) => {setCity(e.target.value)}}/>
                    <datalist id="city">
                      <option value="" selected disabled hidden>-</option>
                      {props.citylist.map(({ name: value }) => <option value={value}>{value}</option>)}
                    </datalist>
                </td>
              </tr>
              <tr>
                <th>Cím</th>
                <th>Telefonszám</th>
                <th>Bankszámlaszám</th>
                <th>Megjegyzés</th>
              </tr>
              <tr>
                <td><input type="text" id="addr" onChange={(e) => {setAddrs(e.target.value)}}/></td>
                <td><input type="text" id="tel" onChange={(e) => {setTelNum(e.target.value)}}/></td>
                <td><input type="text" id="accnum" onChange={(e) => {setAccNum(e.target.value)}}/></td>
                <td><input type="text" id="info" onChange={(e) => {setInfo(e.target.value)}}/></td>
              </tr>
            </tbody>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <button class="btn" onClick={newData}>Új adat felvétele</button>
        </Modal.Footer>
      </Modal>
    );
  }
  else {
    return (
      <Modal
        {...props}
        //{...console.log(props.datbase)}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header>
          <Modal.Title id="contained-modal-title-vcenter">
            Adat Szerkesztése
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <table>
          <thead>
            <tr>
              <th>név</th>
              <th>cégforma</th>
              <th>adószám</th>
              <th>cégjegyzékszám</th>
              <th>település</th>
            </tr>
          </thead>
            <tbody>
              <tr>
                <td><input type="text" id="name" defaultValue={props.datbase[index].name} onChange={(e) => {setName(e.target.value)}}/></td>
                <td>
                  <input list="comp" onChange={(e) => {setComType(e.target.value)}}/>
                  <datalist id="comp">
                    <option value="" selected disabled hidden>-</option>
                    {props.comptypelist.map(({ name: value }) => <option value={value}>{value}</option>)}
                  </datalist>
                </td>
                <td><input type="text" id="tax" defaultValue={props.datbase[index].taxnum} onChange={(e) => {setTaxNum(e.target.value)}}/></td>
                <td><input type="text" id="cpyn" defaultValue={props.datbase[index].comnum} onChange={(e) => {setComNum(e.target.value)}}/></td>
                <td>
                  <input list="city" onChange={(e) => {setCity(e.target.value)}}/>
                  <datalist id="city">
                    <option value="" selected disabled hidden>-</option>
                    {props.citylist.map(({ name: value }) => <option value={value}>{value}</option>)}
                  </datalist>
                </td>
                </tr>
                <tr>
                  <th>cím</th>
                  <th>telefonszám</th>
                  <th>bankszámlaszám</th>
                  <th>megjegyzés</th>
                </tr>
                <tr>
                <td><input type="text" id="addr" defaultValue={props.datbase[index].addrs} onChange={(e) => {setAddrs(e.target.value)}}/></td>
                <td><input type="text" id="tel" defaultValue={props.datbase[index].telnum} onChange={(e) => {setTelNum(e.target.value)}}/></td>
                <td><input type="text" id="accnum" defaultValue={props.datbase[index].accnum} onChange={(e) => {setAccNum(e.target.value)}}/></td>
                <td><input type="text" id="info" defaultValue={props.datbase[index].info} onChange={(e) => {setInfo(e.target.value)}}/></td>
              </tr>
            </tbody>
          </table>
        </Modal.Body>
        <Modal.Footer>
          <button class="btn" onClick={updateData}>Adat frissítése</button>
        </Modal.Footer>
      </Modal>
    );
  }
}

function App() {

  const [databaseList, setDatabaseList] = useState([]);
  const [cityList, setCityList] = useState([]);
  const [compTypeList, setCompTypeList] = useState([]);

  const [modalShow, setModalShow] = useState(false);
  const [idVal, setIdVal] = useState(null);


  useEffect(()=>{
    axios.get('http://localhost:3001/api/db_get').then((databs) =>{
      setDatabaseList(databs.data);
    })
    axios.get('http://localhost:3001/api/city_get').then((citybs) =>{
      setCityList(citybs.data);
    })
    axios.get('http://localhost:3001/api/comp_get').then((compbs) =>{
      setCompTypeList(compbs.data);
    })
  }, []);
  //console.log(cityList);
  return (
    <div>
      <table id="datatable">
      <thead>
        <tr>
          <th>Név</th>
          <th>Cégforma</th>
          <th>Adószám</th>
          <th>Cégjegyzékszám</th>
          <th>Település</th>
          <th>Cím</th>
          <th>Telefonszám</th>
          <th>Bankszámlaszám</th>
          <th>Megjegyzés</th>
        </tr>
      </thead>
      <tbody>
          {databaseList.map((val)=>{
            return (
              <tr>
              <td>{val.name}</td>
              <td>{val.comtype}</td>
              <td>{val.taxnum}</td>
              <td>{val.comnum}</td>
              <td>{val.city}</td>
              <td>{val.addrs}</td>
              <td>{val.telnum}</td>
              <td>{val.accnum}</td>
              <td>{val.info}</td>
              <td><button class="btn" onClick={() => {setIdVal(val.id); setModalShow(true);}}>Szerkeszt</button></td>
              <td><button class="btn" onClick={() => {axios.post('http://localhost:3001/api/delete',{id: val.id}).then(window.setTimeout(function(){window.location.reload()}, 200))/* window.location.reload(false);*/}}>Töröl</button></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <button class="btn" variant="primary" onClick={() => {setModalShow(true); setIdVal(null);}}>Hozzáadás</button>
        <MyVerticallyCenteredModal comptypelist={compTypeList} citylist={cityList} id={idVal} datbase={databaseList} show={modalShow} onHide={() => setModalShow(false)}/>
    </div>
  );
}

export default App;
