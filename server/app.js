const express = require('express');
const bodyparser = require('body-parser')
const app = express();
const sqlite3 = require('sqlite3').verbose();

let db = new sqlite3.Database('./db/companies.db');

app.use(express.json());
app.use(bodyparser.urlencoded({extended: true}));

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  if (req.method === 'OPTIONS') {
    res.header("Access-Control-Allow-Methonds", "PUT, POST, PATCH, DELETE");
    return res.status(200).json({});
  }
  next();
});


app.get("/api/db_get", (req, res) => {
  db.all('SELECT * FROM partners', [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.send(rows);
  });
});

app.get("/api/city_get", (req, res) => {
  db.all('SELECT name FROM cities ORDER BY name ASC', [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.send(rows);
  });
});

app.get("/api/comp_get", (req, res) => {
  db.all('SELECT name FROM companytype ORDER BY name ASC', [], (err, rows) => {
    if (err) {
      throw err;
    }
    res.send(rows);
  });
});

app.post("/api/db_getid", (req, res) => {
  const id = req.body.id;
  console.log(id);
  db.all('SELECT * FROM partners WHERE id=?', id , (err, rows) => {
    if (err) {
      throw err;
    }
    res.send(rows);
  });
});

app.post("/api/edit", (req, res) => {
  const dtbs = req.body;
  console.log(dtbs.id);
  const sql = 'UPDATE partners SET name = ?, comtype = ?, taxnum = ?, comnum = ?, city = ?, addrs = ?, telnum = ?, accnum = ?, info = ? WHERE id = ?';
  db.run(sql, dtbs.name, dtbs.comType, dtbs.taxNum, dtbs.comNum, dtbs.city, dtbs.addrs, dtbs.telNum, dtbs.accNum, dtbs.info, dtbs.id, function(err) {
    if (err) {
      return console.log(err.message);
    }
    // get the last insert id
    console.log(`A row has been updated with rowid ${this.lastID}`);
  });
});

app.post("/api/new_dat", (req, res) => {
  const dtbs = req.body;
  const sql = `INSERT INTO partners(name, comtype, taxnum, comnum, city, addrs, telnum, accnum, info) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)`;
  db.run(sql, dtbs.name, dtbs.comType, dtbs.taxNum, dtbs.comNum, dtbs.city, dtbs.addrs, dtbs.telNum, dtbs.accNum, dtbs.info, function(err) {
    if (err) {
      return console.log(err.message);
    }
    // get the last insert id
    console.log(`A row has been inserted with rowid ${this.lastID}`);
  });
});

app.post("/api/new_city", (req, res) => {
  const dtbs = req.body;
  const sql = `INSERT INTO cities(name) VALUES(?)`;
  db.run(sql, dtbs.name, function(err) {
    if (err) {
      return console.log(err.message);
    }
    // get the last insert id
    console.log(`A city has been inserted with rowid ${this.lastID}`);
  });
});

app.post("/api/new_comtype", (req, res) => {
  const dtbs = req.body;
  const sql = `INSERT INTO companytype(name) VALUES(?)`;
  db.run(sql, dtbs.name, function(err) {
    if (err) {
      return console.log(err.message);
    }
    // get the last insert id
    console.log(`A company type has been inserted with rowid ${this.lastID}`);
  });
});

app.post("/api/delete", (req, res) => {
  const id = req.body.id;
  db.run(`DELETE FROM partners WHERE id=?`, id, function(err) {
  if (err) {
    return console.error(err.message);
  }
  console.log(`Row(s) deleted ${this.changes}`);
  });
});

app.use((req, res, next) => {
  const error = new Error('Not found');
  error.status = 404;
  next(error);
})

app.use((error, req, res, next) => {
  res.status(error.status || 500);
  res.json({
    error: {
      message: error.message
    }
  });
});

module.exports = app;
